# microchip8

A Chip-8 emulator written in micro python, targetting a very basic cyberdeck using a BBC microbit card (https://microbit.org), 
a micro:touch expansion board (https://www.dfrobot.com/product-1942.html) and a monk makes 7-segment module (https://www.monkmakes.com/mb_7_seg.html).

![The protodeck featuring a BBC micro:bit and a neat keyboard](MICROBIT.png?raw=true "The protodeck featuring a BBC micro:bit and a neat keyboard")
