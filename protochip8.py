from microbit import *
import random
import gc
import speech

def getKey():
    i2c_val = i2c.read(0x57, 2)
    i2c_val_int = int.from_bytes(i2c_val, "BIG")
    rval = -1
    if i2c_val_int > 0:
        for j in range(0, 16):
            if keys[j] == i2c_val_int:
                rval = j
                while int.from_bytes(i2c.read(0x57, 2), "BIG") > 0:
                    pass
                break
    return rval


def show_progression(index):
    display.set_pixel(int(index % 5), int((index % 25) / 5), 9)
    if index % 25 == 0:
        display.clear()

i2c.init()
uart.init(tx=pin1)
code = ""
current_opcode = ""
keys = [
    0x2,
    0x80,
    0x40,
    0x20,
    0x200,
    0x100,
    0x400,
    0x4000,
    0x1000,
    0x2000,
    0x1,
    0x4,
    0x10,
    0x800,
    0x8000,
    0x8,
]
index = 0
code_loaded = 0
while True:
    if button_a.is_pressed() and button_b.is_pressed():
        with open("save.hex", "r") as load:
            code = load.read()
            code_loaded = 1
        break
    if button_a.is_pressed():
        current_opcode = ""
        uart.write("/")
    i = getKey()
    if i > -1:
        current_opcode += hex(i)[2]
        uart.write(hex(i)[2])
        if len(current_opcode) == 4:
            code += current_opcode
            if current_opcode == "0000":
                code = ""
                index = 0
                display.clear()
                sleep(200)
                continue
            if current_opcode == "ffff":
                index = 0
                display.clear()
                sleep(200)
                break
            current_opcode = ""
        index += 1
    sleep(50)
code = code[:-4]

if code_loaded == 0:
    with open("save.hex", "w") as save:
        save.write(code)

V0 = 0
V1 = 0
V2 = 0
V3 = 0
V4 = 0
V5 = 0
V6 = 0
V7 = 0
V8 = 0
V9 = 0
Va = 0
Vb = 0
Vc = 0
Vd = 0
Ve = 0
Vf = 0
PC = 0
I = 0
ram = bytearray(4096)
ram[0] = 65
ram[1] = 66
for j in range(0, len(code) / 2):
    val_h = "0x" + code[2 * j]
    val_l = "0x" + code[2 * j + 1]
    ram[j] = int(val_h) * 16 + int(val_l)

code = ""
gc.collect()
uart.write('{}'.format(gc.mem_free()))
sleep(1000)

while PC < 4096:
    opcode = "{:02x}{:02x}".format(ram[PC], ram[PC + 1])
    if opcode == "00e0":
        display.clear()
    if opcode == "00fd":
        break
    if opcode == "00ad":
        indx = 0
        while indx < 256 :
            if ram[int(I)+indx] == 0 :
                break
            else :
                snd = ram[int(I)+indx]
                if snd > 32 and snd < 120 :
                    uart.write(chr(snd+48))
                    speech.say(chr(snd+48))
            indx += 1
    if opcode.startswith("1"):
        val_vh = "0x" + opcode[1]
        val_h = "0x" + opcode[2]
        val_l = "0x" + opcode[3]
        PC = int(val_vh) * 256 + int(val_h) * 16 + int(val_l)
        continue
    if opcode.startswith("3"):
        rnum = opcode[1]
        val_h = "0x" + opcode[2]
        val_l = "0x" + opcode[3]
        if int(globals()["V" + rnum]) == (int(val_h) * 16 + int(val_l)):
            PC += 2
    if opcode.startswith("4"):
        rnum = opcode[1]
        val_h = "0x" + opcode[2]
        val_l = "0x" + opcode[3]
        if int(globals()["V" + rnum]) != (int(val_h) * 16 + int(val_l)):
            PC += 2
    if opcode.startswith("5"):
        rnum1 = "0x" + opcode[1]
        rnum2 = "0x" + opcode[2]
        if int(globals()["V" + rnum1]) == int(globals()["V" + rnum2]):
            PC += 2
    if opcode.startswith("6"):
        rnum = opcode[1]
        val_h = "0x" + opcode[2]
        val_l = "0x" + opcode[3]
        globals()["V" + rnum] = int(val_h) * 16 + int(val_l)
    if opcode.startswith("7"):
        rnum = opcode[1]
        val_h = "0x" + opcode[2]
        val_l = "0x" + opcode[3]
        globals()["V" + rnum] += int(val_h) * 16 + int(val_l)
    if opcode.startswith("8") and opcode[3] == 0:
        rnum1 = opcode[1]
        rnum2 = opcode[2]
        globals()["V" + rnum1] = globals()["V" + rnum2]
    if opcode.startswith("a"):
        val_vh = "0x" + opcode[1]
        val_h = "0x" + opcode[2]
        val_l = "0x" + opcode[3]
        globals()["I"]  = int(val_vh) * 256 + int(val_h) * 16 + int(val_l)
    if opcode.startswith("c"):
        rnum = opcode[1]
        val_h = "0x" + opcode[2]
        val_l = "0x" + opcode[3]
        globals()["V" + rnum] = random.randrange(256) & (int(val_h) * 16 + int(val_l))
    if opcode.startswith("d") :
        rx = opcode[1]
        ry = opcode[2]
        nib = "0x" + opcode[3]
        if opcode[3] != 'f':
            display.set_pixel(globals()["V" + rx], globals()["V" + ry], int(nib))
        else :
            uart.write("{:01x}{:01x}{:01x}{:01x}".format(ram[I],ram[I+1],ram[I+2],ram[I+3]))
    if opcode.startswith("e") :
        rnum = opcode[1]
        if opcode.endswith("9e") :
            if getKey() == int(globals()["V" + rnum]):
                PC += 2
        if opcode.endswith("a1") :
            if getKey() != int(globals()["V" + rnum]):
                PC += 2
    if opcode.startswith("f"):
        rnum = "0x" + opcode[1]
        if opcode.endswith("55") :
            for j in range(0, int(rnum)):
                ram[int(I+j)] = int(globals()["V" + "{:01x}".format(j)])
        if opcode.endswith("1e") :
            globals()["I"]  += int(globals()["V" + rnum])
        if opcode.endswith("65") :
            for j in range(0, int(rnum)):
                globals()["V" + "{:01x}".format(j)] = ram[int(I+j)]
    PC += 2
